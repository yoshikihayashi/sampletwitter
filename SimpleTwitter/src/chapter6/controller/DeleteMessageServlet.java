package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/DeleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        String messageId = request.getParameter("messageId");
        int id = Integer.parseInt(messageId);

        Message message = new Message();
		message.setId(id);
		session.setAttribute("id", id);

		new MessageService().delete(message);
        response.sendRedirect("./");
    }
}
