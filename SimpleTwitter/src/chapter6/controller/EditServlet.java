package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException  {


//      HttpSessionインスタンスの取得
    	HttpSession session = request.getSession();

    	List<String> errorMessages = new ArrayList<String>();
//		メッセージIDを受け取り
		String messageId = request.getParameter("messageId");
		Message message = null;

		if (!StringUtils.isEmpty(messageId) && (messageId.matches("^[0-9]*$"))) {
			int id = Integer.parseInt(messageId);
			session.setAttribute("messageId", id);
			message = new MessageService().edit(id);
		}

		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		session.setAttribute("message", message);
		request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}




    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        Message message = getMessage(request);

        if (!isValid(message, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("edit.jsp");
			return;
		}
        new MessageService().update(message);

        session.setAttribute("erroemessages", message);
        response.sendRedirect("./");
    }

    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

        Message message = new Message();
        message.setId(Integer.parseInt(request.getParameter("messageId")));
        message.setText(request.getParameter("text"));
        return message;
    }

    private boolean isValid(Message message, List<String> errorMessages) {

        String text = message.getText();


        if (StringUtils.isBlank(text)) {
            errorMessages.add("つぶやきを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("つぶやきは140文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }

        return true;
    }
}